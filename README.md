Python - Parsing a JSON file
=

Subject
==
Here enclosed, in this repository are 4 files of interest : 
 - **film.JSON :** A flat file, in JSON format, containing a list of films
 - **test_file_manager.py :** A file executed when you run the tests. This file is interesting for you as it specifies the tests that are executed and il also specifies the results expected. The only modification you are meant to realise in this file is commenting the `self.skipTest`lines.
 - **film_manager.py :** This is were you will write your code
 - **launch_tests.sh :** If you ever have a doubt on what command line is to be used whilst running your tests. 

 Testing
 ==

 Ever heard about the Test Driven Developpement ? The concept is the following :
 - you write unit tests. 
 - you run them and see them fail
 - you write code that completes the tests
 - you run the tests to check if everything is OK. 
 - you refactor, commit and go to the next feature to implement !

 Running a test
 ==

 In this repository, we are using python and it's test library : unittest. 
 You can find some documentation [here](https://docs.python.org/3/library/unittest.html)

In order to start your first test, please execute `./launch_tests.sh`. Feel free to fix this test if you need it. 
It simply executes the command `python -m unittest`

Exercise
==
- Fork, then clone this repository. 
- One by one, validate the tests written 
- Complete the code in order to validate each tests. 
- Do not forget to commit after each successful test. 
- Realise a merge request in the end of the day. 

Evaluation
==
 - One commit per exercise is expected
 - Clean code is the way
 - PEP8 format is required
 - Doctrings are mandatory

![This is the way](https://media.giphy.com/media/7nTiW8rZymfJJLT8OE/giphy.gif)
